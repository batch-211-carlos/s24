//console.log("Hello, World!");

//ES6 Updates

//ES6 is one of the latest version of writing javascript and in fact one of the major update to JS
//let, const - are ES6 updates, these are the new standards of creating variables

//Exponent Operator

const firstNum = 8 ** 2; //exponent operator (**)
console.log(firstNum); //64

const secondNum = Math.pow(8,2);
console.log(secondNum);

let string1 = 'fun';
let string2 = 'Bootcamp';
let string3 = 'Coding';
let string4 = 'Javascript';
let string5 = 'Zuitt';
let string6 = 'Learning';
let string7 = 'Love';
let string8 = 'I';
let string9 = 'is';
let string10 = 'in';


//Mini Activity

let sentence1 = string8+ ' ' + string7+ ' ' + string6+ ' '+string4+ '.';
console.log(sentence1);
let sentence2 = string5+ ' ' + string2+ ' ' + string10 + ' ' + 'so much' +' '+ string1+ '!';
console.log(sentence2);

//'', "" - string literals

//Template Literals
	//allow us to create things using `` (back flicks located before 1 inkeyboard) and easily embed JS expression in it
	/*
		It allows us to write strings without using the concatenation operator (+)
		greatly helps with code readability
	*/


	let sentence = `${string8} ${string7} ${string5} ${string3} ${string2}`;
	console.log(sentence1);

	/*
	${} - is a placeholder that is used to embed JS expressions when creating strings using Template Literals
	*/

	let name = "John";

	//Pre-Template Literal String
	//("") or ('')

	let message = 'Hello' + name + "! Welcome to Programming";

	//String using template literals
	//(``) backticks

	message = `Hello ${name} ! Welcome to Programming.`;
	console.log(message);

	//Multi-Line using Template Literals
	//\n - creating new line

	//another way of creating new line
	const anotherMessage = `
	${name} attended a math competition.
	He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.
	`;
	console.log(anotherMessage);

	let dev = {
		name: 'Peter',
		lasName: 'Parker',
		occupation: 'web developer',
		income: 50000,
		expenses: 60000
	};

	console.log(`${dev.name} is a ${dev.occupation}.`);
	console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`);

	const interestRate = .1;
	const principal = 1000;

	console.log(`The interest on your savings account is ${principal * interestRate}`);


	//Array Destructuring
	/*
	Allows us to unpack elements in arrays into distinct variables

	Syntax: 

		let/const [variableNameA, variableNameB, variableNameC] = array;

	*/

	const fullName = ['Juan', 'Dela', 'Cruz', 'Y', 'Rizal'];

	//Pre-Array destructuring
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);

	//Array Destructuring
	const [firstName, middleName, lastName,,anotherName] = fullName;

	console.log(firstName);
	console.log(middleName);
	console.log(lastName);
	console.log(anotherName);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);

	//Object Destructuring

	/*
	Allows us to unpack properties of objects into distinct variables

	Syntax:

	let/const {propertyNameA,propertyNameB,propertyNameC,} = object
	*/

	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	};

	//Pre-Object Destructuring
	//we can access them using dot notation (.) or bracket notation ([])

	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	const {givenName, maidenName, familyName} = person;

	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);

	//Arrow Functions
	/*
	Compact alternative syntax through traditional functions
	*/

	// => is called anonymous function 
	//this is an alternative syntax
	const hello = () => {
		console.log(`Hello World`);
	}
	hello();

	//traditional function not arrow function
	/*const hello = function hello(){
		console.log('Hello World');
	}
	hello();*/

	//Pre-arrow functions and template literals
	/*
	Syntax:
	function functionName(parameterA,parameterB,parameterC){
	console.log();
	}
	*/

	//Arrow Function
	/*
	Synatx:
	let/const variableName = ([parameterA,parameterB,parameterC]) => {
		console.log;
	}
	*/

	const printFullName = (firstName, middleInitial,lastName) => {
		console.log(`${firstName} ${middleInitial} ${lastName} `)
	}
	printFullName('John', 'D', 'Smith');

	//Arrow Functions with Loops

	//Pre-Arrow Function

	const students = ['John', 'Jane', 'Judy'];

	//"forEach" iterator method for array and loops through each element
	students.forEach(function(student){
		console.log(`${student} is a student`)
	});

	//Arrow Function
	students.forEach((student)=>{
		console.log(`${student} is a student.`)
	});

	//Implicit return Statement
	/*
	There are instances when you can omit the 'return' statement
	This works because even without the 'return' statement JS implicitly adds it for the result of the function
	*/

	//Pre-arrow function

	/*const add = (x,y) => {
		return x + y;
	};*/

	//or

	//{} in an arrow function are code blocks. If an arrow function has a {} or code block, we're going to need to use a return

	//implicit return will only work on arrow function without {}

	//Mini activity

	const add = (x,y) => x+y;

	let total = add(1,2);
	console.log(total);

	const sub = (x,y) => x-y;
	const mul = (x,y) => x*y;
	const div = (x,y) => x/y;

	let totalSub = sub(1,2);
	console.log(totalSub);
	let totalMul = mul(1,2);
	console.log(totalMul);
	let totalDiv = div(1,2);
	console.log(totalDiv);

	//Default Function Argument Value
	//provide a default argument value if none is provided when the function is invoked

	const greet = (name = 'User') => {
		return `Good Evening, ${name}`;
	} 

	console.log(greet()); //if there is no value, it will invoke a default value which is 'User on the parameter'
	console.log(greet('John'));

	//Class-Based object blueprint
	/*
	Allows creation/instantiation of objects using classes as blueprints
	*/

	//Creating a class

	/*
	the constructor is a special method of a class for creating/initializing an object for that class

	Syntax:
	class Classname {
		constructor(objectPropertyA, objectPropertyB, objectPropertyC){
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objectPropertyB;
		}
	}
	*/

	//Constructor Blue print
	//using class and not function
	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	//Instantiate an object from blue print
	/*
	The 'new' operator creates/instantiates a new object with the given arguments as the value of its properties
	*/

	//let/const variableName - new className();

	/*
	creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't reassign it with another data type
	*/
	const myCar = new Car();

	console.log(myCar); //the object was created but it is undefined on the console

	//assigning property sample ".brand"
	myCar.brand = 'Ford';
	myCar.name = 'Everest';
	myCar.year = '1996';

	console.log(myCar);

	//Another way to assign property

	const myNewCar = new Car('Toyota', "Vios", 2021);

	console.log(myNewCar);

	//Mini Acitivity

	class Character {
		constructor(name,role,strength,weakness){
			this.name = name;
			this.role = role;
			this.strength = strength;
			this.weakness = weakness;
		}
	}

	const myCharacter = new Character('Edward Elric', "Full Metal Alchemist", 'Physical Strength', "Hot Headed");

	console.log(myCharacter);